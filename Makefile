BRANCH := $(shell git symbolic-ref --short HEAD | head -c -1)
COMMIT := $(shell git rev-parse HEAD | head -c -1)
DATE := $(shell date -Is -u)

help: ##Show help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

deploy: ##Deploy new configuration
deploy: clean pull check copy reload clean

copy: ##Copy configuration files in place
	/bin/echo "--" | cat - src/prosody.cfg.lua | sponge src/prosody.cfg.lua
	/bin/echo "-- from git branch $(BRANCH) commit $(COMMIT)" | cat - src/prosody.cfg.lua | sponge src/prosody.cfg.lua
	/bin/echo "-- Deployed at $(DATE) by $(USER)" | cat - src/prosody.cfg.lua | sponge src/prosody.cfg.lua
	/bin/echo "--" | cat - src/prosody.cfg.lua | sponge src/prosody.cfg.lua
	sudo install -o root -g prosody -m 0640 src/prosody.cfg.lua /etc/prosody/prosody.cfg.lua
	sudo install -o root -g prosody -m 0640 src/conf.avail/debian.org.cfg.lua /etc/prosody/conf.avail/debian.org.cfg.lua

pull:  ##Pull updates from salsa
	git pull

check: ##Check if the configuration is valid
	sudo -u prosody /usr/bin/prosodyctl --config $(PWD)/src/prosody.cfg.lua check

reload: ##Reload prosody
	sudo /usr/sbin/service prosody reload

clean: ##Clean the repository
	git checkout .
	git clean -fd
